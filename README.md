<p  align="center"><a  href="https://laravel.com"  target="_blank"><img  src="https://ionicframework.com/docs/img/frameworks/angular.svg"  width="400"  alt="Laravel Logo"></a></p>

# CHALLENGE - TRUE TALENT

### Funcionalidad

Esta aplicación te permitirá publicar y administrar frases o citas, basada en dos tipos de roles de usuario (administrador e invitado).

#### Acciones del administrador.

- Inicio de sesión.
- Recuperar contraseña.
- Consultar entradas.
  - Filtrar entradas.
  - Publicar entradas.
  - Rechazar entradas.
  - Comentar entradas.

#### Acciones para los invitados.

- Registro.
- Inicio de sesión.
- Recuperar contraseña.
- Gestionar entradas. - Crear - Retirar
- Comentar entradas.

### Tecnologías

La aplicación esta construida con Angular, ademas de hacer uso de Docker

### Cómo levantar el entorno

Asegurate de tener Docker corriendo y ejecuta el siguiente comando
`docker-compose up --build`

La aplicación quedara expuesta en `http://127.0.0.1:4000/`

#### Importante:

Para probar el aplicativo web clona y levanta el proyecto [technical-test-back-true-talent](https://gitlab.com/AntonioFL/technical-test-back-true-talent)

#### Credenciales del usuario administrador

```
username: admin@test.com
password: 12345678
```
