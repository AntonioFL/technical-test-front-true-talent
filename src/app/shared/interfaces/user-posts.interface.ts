export interface Post {
  id: number;
  content: string;
  status: string;
  comments: Comment[];
  user: User;
}

export interface Comment {
  id?: number;
  user_id?: number;
  post_id: number;
  message: string;
}

export interface User {
  id: number;
  name: string;
  surname: string;
}
