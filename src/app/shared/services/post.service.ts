import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Comment } from "src/app/shared/interfaces/user-posts.interface";
import { environment } from "src/environments/environment";

const headers = new HttpHeaders({
  "Content-Type": "application/json",
  Authorization: `Bearer ${localStorage.getItem("access_token")}`,
});

const requestOptions = { headers: headers };

@Injectable({
  providedIn: "root",
})
export class PostService {
  constructor(private http: HttpClient) {}

  getPostList(): Observable<any> {
    const response = this.http.get(
      `${environment.apiUrl}posts`,
      requestOptions
    );
    return response;
  }

  addComment(comment: Comment): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}comments`,
      comment,
      requestOptions
    );
  }

  updatePost(id, data): Observable<any> {
    return this.http.put(
      `${environment.apiUrl}posts/${id}`,
      data,
      requestOptions
    );
  }

  createPost(data: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}posts`, data, requestOptions);
  }
}
