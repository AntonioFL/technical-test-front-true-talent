import { Component, OnInit } from "@angular/core";
import { UtilityService } from "../shared/services/utility.service";

@Component({
  selector: "app-customer",
  templateUrl: "./customer.component.html",
  styleUrls: ["./customer.component.scss"],
})
export class CustomerComponent implements OnInit {
  constructor(private utilityService: UtilityService) {}

  ngOnInit() {}

  logout() {
    this.utilityService.logout();
  }
}
