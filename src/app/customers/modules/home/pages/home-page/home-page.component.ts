import { Component, OnInit } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { Router } from "@angular/router";
import { Post } from "src/app/shared/interfaces/user-posts.interface";
import { PostService } from "src/app/shared/services/post.service";
import { UtilityService } from "src/app/shared/services/utility.service";
import { AddNewPostComponent } from "../../components/add-new-post/add-new-post.component";

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.scss"],
})
export class HomePageComponent implements OnInit {
  message: string;
  DATA_POSTS: Post[];

  constructor(
    public dialog: MatDialog,
    private postService: PostService,
    private utilityService: UtilityService
  ) {}

  ngOnInit() {
    this.postService.getPostList().subscribe(
      (response) => {
        this.DATA_POSTS = response.posts;
      },
      (err) => {
        if (err.status == 401) {
          this.utilityService.logout();
        }
      }
    );
    // getPostList
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddNewPostComponent, {
      width: "500px",
      data: { message: this.message },
    });

    dialogRef.afterClosed().subscribe((content) => {
      if (content && content != "") {
        this.postService.createPost({ content }).subscribe((response) => {
          this.DATA_POSTS = [...this.DATA_POSTS, response.post];
        });
      }
    });
  }
}
