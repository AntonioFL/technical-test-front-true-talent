import { Component, Input, OnChanges } from "@angular/core";
import { Post } from "src/app/shared/interfaces/user-posts.interface";
import { AfterViewInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { PostService } from "src/app/shared/services/post.service";

@Component({
  selector: "app-post-table",
  templateUrl: "./post-table.component.html",
  styleUrls: ["./post-table.component.scss"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class PostTableComponent implements AfterViewInit, OnChanges {
  displayedColumns: string[] = ["id", "content", "status", "date", "actions"];
  dataSource: MatTableDataSource<Post>;
  newComment: string;
  @Input() dataPost: Post[] = [];

  columnsToDisplayWithExpand = [...this.displayedColumns, "expand"];
  expandedElement: Post | null;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private postService: PostService) {
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnChanges(changes: any) {
    this.dataSource = new MatTableDataSource(this.dataPost);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource = new MatTableDataSource(this.dataPost);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addComment(post_id, index) {
    let comment = {
      message: this.newComment,
      post_id: post_id,
    };

    this.postService.addComment(comment).subscribe((response) => {
      this.dataPost[index].comments.push(response.comment);
    });

    this.clearInput();
  }

  updateStatus(id, index, $event) {
    $event.stopPropagation();
    let status = "removed";
    this.postService.updatePost(id, { status }).subscribe((response) => {
      this.dataPost[index].status = status;
    });
  }

  togglePanel($event, element) {
    this.expandedElement = this.expandedElement === element ? null : element;
    $event.stopPropagation();
    this.clearInput();
  }

  clearInput() {
    this.newComment = "";
  }
}
