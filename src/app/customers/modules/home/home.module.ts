import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HomeRoutingModule } from "./home-routing.module";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { PostTableComponent } from "./components/post-table/post-table.component";
import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
} from "@angular/material";
import { FormsModule } from "@angular/forms";
import { AddNewPostComponent } from "./components/add-new-post/add-new-post.component";

@NgModule({
  declarations: [HomePageComponent, PostTableComponent, AddNewPostComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    MatDialogModule,
  ],
  entryComponents: [AddNewPostComponent],
})
export class HomeModule {}
