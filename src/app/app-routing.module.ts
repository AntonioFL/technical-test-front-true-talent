import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminGuard, GuestGuard, LoginGuard } from "./shared/guards";

const routes: Routes = [
  {
    path: "auth",
    loadChildren: () => import("./auth/auth.module").then((m) => m.AuthModule),
    canActivateChild: [LoginGuard],
  },
  {
    path: "",
    redirectTo: "/auth",
    pathMatch: "full",
  },
  {
    path: "admin",
    loadChildren: () =>
      import("./admin/admin.module").then((m) => m.AdminModule),
    canActivateChild: [AdminGuard],
  },
  {
    path: "guest",
    loadChildren: () =>
      import("./customers/customer.module").then((m) => m.CustomerModule),
    canActivateChild: [GuestGuard],
  },
  {
    path: "**",
    redirectTo: "admin",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
