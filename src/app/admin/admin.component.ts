import { Component, OnInit } from "@angular/core";
import { UtilityService } from "../shared/services/utility.service";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"],
})
export class AdminComponent implements OnInit {
  showFiller = true;

  constructor(private utilityService: UtilityService) {}

  ngOnInit() {}

  logout() {
    this.utilityService.logout();
  }
}
