import { Component, OnInit } from "@angular/core";
import { AfterViewInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Post } from "src/app/shared/interfaces/user-posts.interface";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { PostService } from "src/app/shared/services/post.service";
import { Router } from "@angular/router";
import { UtilityService } from "src/app/shared/services/utility.service";

@Component({
  selector: "app-post-table",
  templateUrl: "./post-table.component.html",
  styleUrls: ["./post-table.component.scss"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class PostTableComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = [
    "id",
    "content",
    "status",
    "user",
    "date",
    "actions",
  ];
  dataSource: MatTableDataSource<Post>;
  newComment: string;
  DATA_POSTS: Post[];

  columnsToDisplayWithExpand = [...this.displayedColumns, "expand"];
  expandedElement: Post | null;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private postService: PostService,
    private utilityService: UtilityService
  ) {
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.postService.getPostList().subscribe(
      (response) => {
        this.DATA_POSTS = response.posts;
        this.dataSource = new MatTableDataSource(this.DATA_POSTS);
      },
      (err) => {
        if (err.status == 401) {
          this.utilityService.logout();
        }
      }
    );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addComment(post_id, index) {
    let comment = {
      message: this.newComment,
      post_id: post_id,
    };

    this.postService.addComment(comment).subscribe((response) => {
      this.DATA_POSTS[index].comments.push(response.comment);
    });

    this.clearInput();
  }

  togglePanel($event, element) {
    this.expandedElement = this.expandedElement === element ? null : element;
    $event.stopPropagation();
    this.clearInput();
  }

  clearInput() {
    this.newComment = "";
  }

  updateStatus(id, status, index, $event) {
    $event.stopPropagation();
    this.postService.updatePost(id, { status }).subscribe((response) => {
      this.DATA_POSTS[index].status = status;
    });
  }
}
