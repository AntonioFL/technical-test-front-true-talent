import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PostsRoutingModule } from "./posts-routing.module";
import { PostPageComponent } from "./posts/pages/post-page/post-page.component";
import { PostTableComponent } from "./posts/components/post-table/post-table.component";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatTableModule } from "@angular/material/table";
import {
  MatButtonModule,
  MatIconModule,
  MatPaginatorModule,
} from "@angular/material";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [PostPageComponent, PostTableComponent],
  imports: [
    CommonModule,
    PostsRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
  ],
})
export class PostsModule {}
