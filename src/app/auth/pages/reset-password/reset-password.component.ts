import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.scss"],
})
export class ResetPasswordComponent implements OnInit {
  public validForm = true;
  public resetPasswordForm: FormGroup;
  public errorMessage = "Ingresa los datos";
  private token: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.resetPasswordForm = this.createResetPasswordForm();

    this.route.queryParams.subscribe((params) => {
      this.token = params.token;
    });
  }

  createResetPasswordForm() {
    return this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
      password_confirmation: ["", Validators.required],
    });
  }

  reset() {
    if (this.resetPasswordForm.invalid) {
      this.validForm = false;
      return;
    }

    const data = this.resetPasswordForm.value;
    data.token = this.token;

    this.authService.reset(data).subscribe(
      (response: any) => {
        if (response) {
          this.router.navigate(["/auth"]);
        } else {
          this.validForm = false;
        }
      },
      (err) => {
        this.errorMessage = err.error.message;
        this.validForm = false;
      }
    );
  }
}
