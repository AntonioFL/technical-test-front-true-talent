import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-login-page",
  templateUrl: "./login-page.component.html",
  styleUrls: ["./login-page.component.scss"],
})
export class LoginPageComponent implements OnInit {
  loginForm: FormGroup;
  validForm = true;
  token: string;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loginForm = this.createLoginForm();
  }

  login() {
    if (this.loginForm.invalid) {
      this.validForm = false;
      return;
    }

    this.authService.login(this.loginForm.value).subscribe(
      (response: any) => {
        if (response && response.access_token) {
          localStorage.setItem("access_token", response.access_token);
          localStorage.setItem("role", response.role_id);

          if (response.role_id == 1) {
            this.router.navigate(["/admin"]);
          } else {
            this.router.navigate(["/guest"]);
          }
        } else {
          this.validForm = false;
        }
      },
      (err) => {
        if (err.status === 401) {
          this.validForm = false;
        }
      }
    );
  }

  createLoginForm() {
    return this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
    });
  }
}
