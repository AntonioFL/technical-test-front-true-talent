import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  validForm = true;
  token: string;
  errorMessage: string = "Enter your data";
  @Output() triggerFlip = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.registerForm = this.createRegisterForm();
  }

  rotateCard() {
    this.triggerFlip.emit("login");
  }

  register() {
    if (this.registerForm.invalid) {
      this.validForm = false;
      return;
    }

    this.authService.register(this.registerForm.value).subscribe(
      (response: any) => {
        console.log("register -> response", response);
        console.log("register -> status", response.status);
        // return;

        if (response && response.access_token) {
          localStorage.setItem("access_token", response.access_token);
          localStorage.setItem("role", response.role_id);

          if (response.role_id == 1) {
            this.router.navigate(["/admin"]);
          } else {
            this.router.navigate(["/guest"]);
          }
        } else {
          this.errorMessage = "Incorrect data";
          this.validForm = false;
        }
      },
      (err) => {
        console.log(err.error.message);
        this.errorMessage = err.error.message;
        this.validForm = false;
      }
    );
  }

  createRegisterForm() {
    return this.formBuilder.group({
      name: ["", Validators.required],
      surname: ["", Validators.required],
      email: ["", Validators.required],
      password: ["", Validators.required],
    });
  }
}
