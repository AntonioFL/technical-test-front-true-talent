import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-recover-password",
  templateUrl: "./recover-password.component.html",
  styleUrls: ["./recover-password.component.scss"],
})
export class RecoverPasswordComponent implements OnInit {
  recoverPasswordForm: FormGroup;
  validForm = true;
  showSuccess = false;
  token: string;
  errorMessage: string = "Enter your data";
  successMessage: string;
  @Output() triggerFlip = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder, // private authService: AuthService, // private router: Router
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.recoverPasswordForm = this.createLoginForm();
  }

  rotateCard() {
    this.triggerFlip.emit("login");
  }

  recoverPassword() {
    if (this.recoverPasswordForm.invalid) {
      this.validForm = false;
      return;
    }
  }

  createLoginForm() {
    return this.formBuilder.group({
      email: ["", Validators.required],
    });
  }

  sendResetEmail() {
    if (this.recoverPasswordForm.invalid) {
      this.validForm = false;
      return;
    }

    this.authService.sendResetEmail(this.recoverPasswordForm.value).subscribe(
      (response: any) => {
        console.log("register -> response", response);
        this.showSuccess = false;
        if (!response) return;
        console.log("--->");

        this.successMessage = response.message;
        this.showSuccess = true;
        this.validForm = true;
        this.recoverPasswordForm.reset();
      },
      (err) => {
        console.log(err.error.message);
        this.errorMessage = err.error.message;
        this.validForm = false;
      }
    );
  }
}
