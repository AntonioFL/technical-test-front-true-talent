import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { Component, OnChanges, OnInit } from "@angular/core";

@Component({
  selector: "app-rotating-card",
  templateUrl: "./rotating-card.component.html",
  styleUrls: ["./rotating-card.component.scss"],
  animations: [
    trigger("cardFlip", [
      state(
        "default",
        style({
          transform: "none",
        })
      ),
      state(
        "flipped",
        style({
          transform: "rotateY(180deg)",
        })
      ),
      transition("default => flipped", [animate("400ms")]),
      transition("flipped => default", [animate("200ms")]),
    ]),
  ],
})
export class RotatingCardComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  public state: string = "default";
  public secondView: string = "login";

  cardClicked(event) {
    this.secondView = event;

    if (this.state === "default") {
      this.state = "flipped";
    } else {
      this.state = "default";
    }
  }
}
