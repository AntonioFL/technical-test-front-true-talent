import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  validForm = true;
  token: string;
  @Output() triggerFlip = new EventEmitter<string>();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  rotateCard(view) {
    this.triggerFlip.emit(view);
  }

  ngOnInit() {
    this.loginForm = this.createLoginForm();
  }

  login() {
    if (this.loginForm.invalid) {
      this.validForm = false;
      return;
    }

    this.authService.login(this.loginForm.value).subscribe(
      (response: any) => {
        if (response && response.access_token) {
          localStorage.setItem("access_token", response.access_token);
          localStorage.setItem("role", response.role_id);

          if (response.role_id == 1) {
            this.router.navigate(["/admin"]);
          } else {
            this.router.navigate(["/guest"]);
          }
        } else {
          this.validForm = false;
        }
      },
      (err) => {
        if (err.status === 401) {
          this.validForm = false;
        }
      }
    );
  }

  createLoginForm() {
    return this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
    });
  }
}
