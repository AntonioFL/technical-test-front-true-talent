import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

const headers = new HttpHeaders({
  "Content-Type": "application/json",
});

const requestOptions = { headers: headers };

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(credentials): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}login`,
      credentials,
      requestOptions
    );
  }

  register(data): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}register`,
      data,
      requestOptions
    );
  }

  reset(credentials): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}password/reset`,
      credentials,
      requestOptions
    );
  }

  sendResetEmail(credentials) {
    return this.http.post(
      `${environment.apiUrl}password/email`,
      credentials,
      requestOptions
    );
  }
}
